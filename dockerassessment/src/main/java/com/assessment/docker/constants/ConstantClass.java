package com.assessment.docker.constants;

public class ConstantClass
{
    private ConstantClass() {
        throw new IllegalStateException("ConstantClass");
    }

    public static final String URI_ASSESSMENT_CONTROLLER = "/assessmentcontroller";
    public static final String URI_GET_MESSAGE = "/message";

}
