package com.assessment.docker.controller;

import com.assessment.docker.constants.ConstantClass;
import com.assessment.docker.exception.ResouceNotFoundException;
import com.assessment.docker.model.Employee;
import com.assessment.docker.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ConstantClass.URI_ASSESSMENT_CONTROLLER)
public class ControllerClass {

    @Autowired
    private EmployeeRepository employeeRepository;

    /** QUESTION 1**/
    @GetMapping(value = ConstantClass.URI_GET_MESSAGE)
    public String getMessage() {
        return "Hello!";
    }

    /** QUESTION 3**/
    @PostMapping("/employees")
    public Employee addEmployee(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    /** QUESTION 3**/
    @PutMapping("employees/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") Integer employeeId,
                                                   @RequestBody Employee employeeDetails) {
        Employee employee = employeeRepository.findById(employeeId)
                .orElseThrow(() -> new ResouceNotFoundException("Employee not found for this id :: " + employeeId));
        employee.setName(employeeDetails.getName());
        final Employee updatedEmployee = employeeRepository.save(employee);
        return ResponseEntity.ok(updatedEmployee);

    }

}
